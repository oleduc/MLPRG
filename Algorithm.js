/**
 * Created by Olivier Leduc on 19/05/14.
 */
var util = require('util');

DEBUG = false;
var generationConfig = {
    draws : {
        factors : {
            f1: 1,
            f2: 1,
            f3: 3
        },
        blocks : 5
    }
};

var randomFactors = setupFactors();
var randomBlocks = setupBlocks(randomFactors);

console.time("getResults");
var results = generateSelection(randomFactors,randomBlocks,generationConfig);
console.timeEnd("getResults");

console.log(util.inspect(results, false, 10));

function generateSelection(factors,blocks,config){
    var selection = {};
    for(var property in factors){

        if(factors.hasOwnProperty(property)){
            var current = factors[property];

            if(!current[0].conditions){
                selection[property] = drawSimple(current,config.draws.factors[property]);
            } else {
                var parents = [];
                for(var name in selection){
                    for(var parentIndex in selection[name]){
                        parents.push({type:name,_id:factors[name][parentIndex]._id});
                    }
                }
                selection[property] = drawComplex(current,parents,config.draws.factors[property]);
            }

        }

    }
    return selection;
}

/**
 * Draw a factor or block with X conditions
 * @param array of candidates
 * @param parents
 * @param numbersToDraw
 * @returns {Array}
 */
function drawComplex(array,parents,numbersToDraw){
    var chances = [];
    var selected = [];

    var totalGood = 0;
    var totalBad = 0;
    //For each element in the array of possible picks
    for(var ii = 0; ii < array.length; ii++){
        var disqualified = false;
        var probabilities = [];

        //For each parents
        for(var pi in parents){
            var chanceObj = {};
            var chanceObjFound = false;

            //Find the right chance object in the array of condition of type "Parents[pi].type"
            for(var chanceIndex in array[ii].conditions[parents[pi].type]){
                var currentObj = array[ii].conditions[parents[pi].type][chanceIndex];
                if(currentObj._id == parents[pi]._id) {
                    chanceObj = currentObj;
                    chanceObjFound = true;
                    break;
                }
            }
            if(chanceObjFound){
                //If any chance equals zero, then there is no need to evaluate the other probabilities
                if(chanceObj.chance == 0) {
                    totalBad++;
                    disqualified = true;
                    break;
                }

                //Chance is not zero, push to probabilities array
                probabilities.push(chanceObj.chance);
                //Keep looping
            } else {
                totalBad++;
                //This element has no chance defined for this parent, default to zero
                disqualified = true;
                break;
            }
        }

        if(!disqualified){
            totalGood++;
            var total = 0;

            //Calculate average
            for(var iii = 0; iii < probabilities.length; iii++){
                total += probabilities[iii];
            }
            chances.push({index:ii,chances: Math.floor(total / probabilities.length)});
        }

    }
    if(DEBUG){
        console.log('///////////////////////COMPLEX CHANCES AVERAGING STATS///////////////////////');
        console.log('CHANCES: ',chances);
        console.log('PARENTS: ',parents);
        console.log('Total GOOD: ',totalGood);
        console.log('Total Bad: ',totalBad);
        console.log('Total: ',ii);
        console.log('/////////////////////////////////////////////////////////////////////////////');
        console.log('///////////////////////DRAW TEST///////////////////////');
        var stats = {};
        for(var i = 0; i < 1000000; i++){
            var draw = drawRandomWithAliasMethod(chances);
            if(typeof stats[draw] != 'number' ) stats[draw] = 0;
            stats[draw]++;
        }
        for(statI in stats){
            console.log('INDEX: '+statI+' ( '+((stats[statI]/1000000)*100)+'%)')
        }
        console.log('////////////////////////////////////////////////////////');
    }

    //Draw for the required number of times
    for(var i = 0;i<numbersToDraw;i++){
        var drawnIndex;
        do{
            drawnIndex = drawRandomWithAliasMethod(chances);
        }while(isInArray(selected,drawnIndex));
        selected.push(drawnIndex);
    }

    return selected;
}

/**
 * Draw a factor with no conditions
 * @param array
 * @param numbersToDraw
 * @returns {Array}
 */
function drawSimple(array,numbersToDraw){
    var numbers = [];
    for(var i = 0; i < numbersToDraw; i++){
        do{
            var draw = Math.floor((Math.random() * array.length));
        }while(isInArray(numbers,draw));
        numbers.push(draw);
    }
    return numbers;
}

/**
 * Draws a index of array of object with format [{chances:NUMBER,index:NUMBER}]
 * @param arrayOfProbabilities Ex: [{chances:NUMBER,index:NUMBER}]
 * @returns NUMBER index property of winning object
 */
function drawRandomWithAliasMethod(arrayOfProbabilities){
    var representativeArray = [];
    for(var i = 0;i < arrayOfProbabilities.length; i++){
        for(var ii = 0; ii < arrayOfProbabilities[i].chances; ii++){
            //Expand probabilities into a representative array (Fast but memory intensive with large arrays)
            representativeArray.push(arrayOfProbabilities[i].index);
        }
    }
    return representativeArray[Math.floor(Math.random() * (representativeArray.length - 1) + 1)];
}

function isInArray(array,value){
    for(var i = 0;i < array.length; i++){
        if(array[i] === value)
            return true;
    }
    return false;
}


/**
 * Function to generate a random data set
 * @return Object With properties of array of random data
 */
function setupFactors(){
    var factors = {
        f1 : [],
        f2 : [],
        f3 : []
    };
    for(var i = 0; i < Math.floor((Math.random() * (5 - 3) + 3)); i++){
        factors.f1.push({
            _id     : 'f1-'+i,
            name : "first factor "+i
        });
        factors.f2.push({
            _id     : 'f2-'+i,
            name : "secondary factor "+i
        });
    }

    for(var i = 0; i < Math.floor((Math.random() * (50 - 5) + 5)); i++){
        var conditions = generateConditions(factors,'f3');
        factors.f3.push({
            _id     : 'f3-'+i,
            name : "tertiary factor "+i,
            description : conditions.description,
            conditions: conditions.list
        });
    }
    return factors;
}

function setupBlocks(factors){
    var blocks = [];
    for(var i = 0; i < Math.floor((Math.random() * (100 - 25) + 25)); i++){
        var conditions = generateConditions(factors,'');
        blocks.push({
            _id: 'block-'+i,
            name: 'Friendly block number: '+i,
            description : conditions.description,
            conditions: conditions.list
        });
    }
    return blocks;
}

function generateConditions(data,currentProperty){
    var conditions = {};
    var conditionsDescription = '';
    var arrayCounter = 0;
    for(var property in data){
        if(property !== currentProperty && data.hasOwnProperty(property)){
            var array = data[property];
            conditions[property] = [];

             for(var i = 0; i < array.length; i++){
                var realFeeling = Math.floor((Math.random() * 100));

                 var feeling = true;
                 if(realFeeling < 25){
                     realFeeling = 0;
                     feeling = false;
                 }

                 if(feeling == 0){
                     if(conditionsDescription == ''){
                         conditionsDescription = 'I hate "'+array[i].name+'" ';
                     } else {
                         conditionsDescription += 'and "'+array[i].name+'" '
                     }
                 }

                 conditions[property].push({
                     _id:array[i]._id,
                     chance: realFeeling
                 });
             }

            arrayCounter++;
        }
    }
    return {
        description: conditionsDescription,
        list: conditions
    };
}